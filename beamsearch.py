#!/usr/bin/env python2.7

# Stefan Behr
# LING 572
# Homework 6

import sys, utils

if __name__ == "__main__":
	help = """\
Usage information:

	argv[1] is a test data file
	argv[2] is a boundary file
	argv[3] is a model file (text format)
	argv[4] is for writing classifier output
	argv[5] is the beam size
	argv[6] is topN
	argv[7] is topK\
"""
	MIN_ARGS = 7
	if len(sys.argv) - 1 < MIN_ARGS:
		print "{0} args required, {1} given.".format(MIN_ARGS, len(sys.argv) - 1)
		print help
		exit()
	else:
		test_path = sys.argv[1]
		boundary_path = sys.argv[2]
		model_path = sys.argv[3]
		output_path = sys.argv[4]
		beam_size = float(sys.argv[5])
		topN = int(sys.argv[6])
		topK = int(sys.argv[7])

		# load model and boundaries
		model = utils.Model(model_path)
		boundaries = utils.load_boundaries(boundary_path)

		tagged = 0
		tagged_correctly = 0

		# open and iterate through test file
		with open(test_path) as test_file:
			with open(output_path, "w") as output_file:
				for boundary in boundaries:
					# initialize tree and add first level
					sentence = []	# holds sentence with gold tags for future output
					tree = []		# beam search tree
					word, gold_tag, history = utils.process_instance(test_file.readline().strip())
					topN_tags = utils.topn_by_value(model.tag_probabilities(history), topN)
					tree.append([utils.BeamNode(tag, prob) for tag, prob in topN_tags.items()])
					sentence.append((word, gold_tag))

					# walk through rest of sentence
					for i in xrange(1, boundary):
						word, gold_tag, history = utils.process_instance(test_file.readline().strip())
						sentence.append((word, gold_tag))
						new_nodes = []
						for node in tree[i - 1]:
							# augment instance feature vector
							tagMin1 = node.tag	# tag i-1
							tagMin2 = "BOS"		# tag i-2
							if node.parent:
								tagMin2 = node.parent.tag
							prevT = "prevT={0}".format(tagMin1)
							prev2T = "prevTwoTags={0}+{1}".format(tagMin2, tagMin1)
							path_history = history + [prevT, prev2T]

							# use augmented feature vector to get tag probs, limit to topN
							topN_tags = utils.topn_by_value(model.tag_probabilities(path_history), topN)
							# create BeamNodes for topN tags, add to current tree level candidates
							new_nodes.extend([utils.BeamNode(tag, prob, node) for tag, prob in topN_tags.items()])

						# prune topK nodes and store
						tree.append(utils.prune_nodes(new_nodes, topK, beam_size))

					# done with sentence, find best path and recover tag sequence
					best_leaf = max(tree[-1], key=utils.BeamNode.get_pprob)
					best_sequence = best_leaf.get_tag_prob_sequence()

					# print system output
					for i, instance in enumerate(sentence):
						word, gold_tag = instance
						sys_tag, prob = best_sequence[i]
						print >> output_file, "{0} {1} {2} {3}".format(word, gold_tag, sys_tag, prob)
						tagged += 1
						if gold_tag == sys_tag:
							tagged_correctly += 1

		print "Test accuracy={0}".format(float(tagged_correctly) / tagged)