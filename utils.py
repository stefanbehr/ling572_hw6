"""Utility functions for LING 572, homework 4."""

# Stefan Behr
# LING 572
# Homework 6

from math import exp, log

class Model:
	"""
	MaxEnt model class for storing and accessing MaxEnt feature weights.
	"""
	class_prefix = "FEATURES FOR CLASS "
	word_prefix = "curW="

	def __init__(self, model_path):
		"""
		Loads model file into Model object, including tag 
		list and word-tag dictionary.
		"""
		self.weights = {}
		self.tags = []

		with open(model_path) as model_file:
			for line in model_file:
				line = line.strip()
				if line:
					if line.startswith(Model.class_prefix):
						current_class = line[len(Model.class_prefix):]
						self.tags.append(current_class)
						self.weights[current_class] = {}
					else:
						feature, weight = line.split()
						weight = float(weight)
						self.weights[current_class][feature] = weight

	def tag_probabilities(self, vector):
		"""
		Given a vector of features, return a probability distribution 
		over all tags in the model given the feature vector.
		"""
		default = "<default>"
		Z = 0
		probabilities = {}
		for class_label in self.weights:
			total = self.weights[class_label][default]
			for feature in vector:
				total += self.weights[class_label].get(feature, 0)
			total = exp(total)
			Z += total
			probabilities[class_label] = total
		probabilities = {class_label: float(total) / Z for class_label, total in probabilities.items()}
		return probabilities

class BeamNode:
	"""
	Class for nodes used in beam search trees.
	"""
	def __init__(self, tag, prob, parent=None):
		self.tag = tag
		self.lprob = prob	# local tag probability given history
		self.parent = parent	# either a BeamNode or None if self is root
		if self.parent:
			self.pprob = self.lprob * self.parent.pprob	# probability of tree path leading to and including this node
		else:
			self.pprob = self.lprob

	def get_pprob(self):
		return self.pprob

	def get_tag_prob_sequence(self):
		"""
		Return the tag sequence associated with the path from 
		a node up to the root of the tree containing it, with tags 
		ordered from the root to the node. Each tag is associated 
		with its probability.
		"""
		return list(reversed(self.tag_prob_sequence_helper()))

	def tag_prob_sequence_helper(self):
		"""
		Returns the sequence of tags (and their probabilities) associated 
		with a path from a node up to its root.
		"""
		seq = [(self.tag, self.lprob)]
		if self.parent:
			seq.extend(BeamNode.tag_prob_sequence_helper(self.parent))
		return seq

def prune_nodes(nodes, topK, beam_size):
	"""
	Takes a list of BeamNodes and returns the topK nodes 
	with highest path probability, minus any that fall outside 
	of the given beam size.
	"""
	max_prob = max(nodes, key=BeamNode.get_pprob).get_pprob()
	nodes = sorted(nodes, key=BeamNode.get_pprob, reverse=True)[:topK]
	fits_beam = make_beam(beam_size, max_prob)
	return filter(fits_beam, nodes)

def make_beam(beam_size, maxp):
	"""
	Returns function that takes a node probability and 
	checks its probability against a beam size and max probability.
	"""
	def beam_check(node):
		return lg(node.get_pprob()) + beam_size >= lg(maxp)
	return beam_check

def lg(x):
	"""
	Base 10 log
	"""
	return log(x, 10)

def topn_by_value(d, n):
	"""
	Given a dictionary with sortable values, 
	return dictionary containing only the top n 
	values with their respective keys.
	"""
	return {k: v for v, k in sorted([(v, k) for k, v in d.items()], reverse=True)[:n]}

def load_boundaries(boundary_path):
	"""
	Given a path to a sentence boundary file, return a 
	list of the sentence boundaries in the file, with order 
	preserved.
	"""
	with open(boundary_path) as boundary_file:
		boundaries = [int(boundary) for boundary in boundary_file.read().strip().split("\n")]
	return boundaries

def process_instance(test_line):
	"""
	Given a string in the following format:

		"<word> <tag> <f1> 1 <f2> 1 ... <fn> 1"

	Returns tuple of the form (<word>, <tag>, [<f1>, <f2>, ..., <fn>]).
	"""
	test_line = test_line.split()
	return (test_line[0], test_line[1], [item for item in test_line[2:] if item != "1"])