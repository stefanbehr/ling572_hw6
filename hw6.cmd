Executable = beamsearch_maxent.sh
Universe   = vanilla
getenv     = true
input      = 
output     = acc1
error      = test1.error
Log        = test1.log
arguments  = /dropbox/12-13/572/hw6/examples/sec19_21.txt /dropbox/12-13/572/hw6/examples/sec19_21.boundary /dropbox/12-13/572/hw6/examples/m1.txt sys1 0 1 1
transfer_executable = false
request_memory = 2*1024
Queue

output     = acc2
error      = test2.error
Log        = test2.log
arguments  = /dropbox/12-13/572/hw6/examples/sec19_21.txt /dropbox/12-13/572/hw6/examples/sec19_21.boundary /dropbox/12-13/572/hw6/examples/m1.txt sys2 1 3 5
Queue

output     = acc3
error      = test3.error
Log        = test3.log
arguments  = /dropbox/12-13/572/hw6/examples/sec19_21.txt /dropbox/12-13/572/hw6/examples/sec19_21.boundary /dropbox/12-13/572/hw6/examples/m1.txt sys3 2 5 10
Queue

output     = acc4
error      = test4.error
Log        = test4.log
arguments  = /dropbox/12-13/572/hw6/examples/sec19_21.txt /dropbox/12-13/572/hw6/examples/sec19_21.boundary /dropbox/12-13/572/hw6/examples/m1.txt sys4 3 10 100
Queue