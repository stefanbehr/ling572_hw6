#!/usr/bin/env bash

# Stefan Behr
# LING 572
# Homework 6

# $1 is test data
# $2 is a boundary file
# $3 is a model_file (same format as prev hw)
# $4 is sys_output (output file)
# $5 is beam size
# $6 is topN
# $7 is topK

python2.7 beamsearch.py $1 $2 $3 $4 $5 $6 $7